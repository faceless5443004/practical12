# Название вашего проекта

practical12

Краткое описание проекта.
Практическое занятие #12: Разработка API блога с использованием DRF
API для управления статьями в блоге с помощью Django REST Framework.

## Установка

1. Склонируйте репозиторий:

   ```bash
   git clone git@gitlab.com:faceless5443004/practical12.git>
   ```

2. Создайте виртуальное окружение (рекомендуется):

   ```bash
   python -m venv venv
   ```

3. Активируйте виртуальное окружение:

   - Для Windows:

   ```bash
   venv\Scripts\activate
   ```

   - Для macOS и Linux:

   ```bash
   source venv/bin/activate
   ```

4. Установите зависимости:

   ```bash
   pip install -r requirements.txt
   ```

5. Примените миграции:

   ```bash
   python manage.py migrate
   ```

## Использование

ссылки которые работает
http://127.0.0.1:8000/api
http://127.0.0.1:8000/api/posts/
http://127.0.0.1:8000/api/posts/1/
http://127.0.0.1:8000/api/comments/1
http://127.0.0.1:8000/api/users/
http://127.0.0.1:8000/api/users/1/

## Дополнительные инструкции

- [Документация Django](https://docs.djangoproject.com/en/3.2/)
- [Документация Django REST Framework](https://www.django-rest-framework.org/)
